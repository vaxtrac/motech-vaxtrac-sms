package org.vaxtrac.motech.sms.event;

import org.motechproject.event.MotechEvent;
import org.motechproject.event.listener.EventRelay;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;


@Component
public class EventEmitter {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private EventRelay eventRelay;

    public EventEmitter() { }

    public EventEmitter(EventRelay eventRelay) {
	this.eventRelay = eventRelay;
    }

    public void emitEvent(String subject, Map<String, Object> eventParams) {
	MotechEvent motechEvent = new MotechEvent(subject, eventParams);
	eventRelay.sendEventMessage(motechEvent);
	String message = "Emitted an event with subject " + subject;
	logger.info(message);
    }
 
    public void emitDisabledTaskEvent(String message) {
	Map<String, Object> eventParams = new HashMap<String, Object>();
	eventParams.put(EventParams.MESSAGE, message);
	emitEvent(EventSubjects.TASK_DISABLED, eventParams);
    }
}
