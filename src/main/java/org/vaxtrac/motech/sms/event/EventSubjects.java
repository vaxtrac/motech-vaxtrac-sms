package org.vaxtrac.motech.sms.event;

public final class EventSubjects {
    private EventSubjects() {
    }
    
    public static final String BASE = "org.vaxtrac.motech.sms.";
    
    public static final String TASK_DISABLED = BASE + "task_disabled";
    public static final String TASK_MODULE_NOTIFICATION = "org.motechproject.message";
    public static final String SEND_RELAIS_SMS = BASE + "send_relais_sms";
    public static final String SCHEDULE_RELAIS_SMS = BASE + "schedule_relais_sms";
    public static final String COMPUTE_SMS_REMINDER_DATE = BASE + "compute_sms_reminder_date";
    public static final String LOG_PATIENT = BASE + "log_patient";
    public static final String LOG_GUARDIAN = BASE + "log_guardian";   
}
