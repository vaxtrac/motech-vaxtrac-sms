package org.vaxtrac.motech.sms.event;

public final class EventParams {
    private EventParams () {
    }
    
    public static final String CASE_ID = "case_id";
    public static final String DOSE_CASE_ID = "dose_case_id";
    public static final String DOB = "date_of_birth";
    public static final String RECORDED_VACCINES = "recorded_vaccines";
    public static final String REPORTED_VACCINES = "reported_vaccines";
    public static final String GENDER = "gender";
    public static final String DATE_OF_ENROLLMENT = "event_date";
    public static final String ORG_UNIT_ID = "org_unit_id";
    public static final String PATIENT_AGE = "patient_age";
    public static final String PATIENT_NAME = "patient_name";
    public static final String CONTACT_NUMBER = "contact_number";
    public static final String REMINDER_DATE = "reminder_date";
    public static final String APPOINTMENT_DATE = "appointment_date";
    public static final String CALLBACK_DATE = "callback_date";
    public static final String REMINDER_OFFSET_DAYS = "reminder_offset_days";
    public static final String DELIVERY_TIME = "delivery_time";
    public static final String TIMEZONE = "timezone";
    public static final String HOUR = "hour";
    public static final String MINUTES = "minutes";
    public static final String MESSAGE = "message";
    public static final String PATIENT_ID = "patient_id";
    public static final String USER_ID = "user_id";
    public static final String VACCINE_NAME = "vaccine_name";
    public static final String SERIES = "series";
    public static final String VILLAGE = "village";
    public static final String EXTERNAL_ID = "external_id";
    public static final String STAGE = "stage";
    public static final String DATE = "date";
    public static final String TIMESTAMP = "timestamp";
    
    
    public static final String RELAIS_NAME = "relais_name";
    public static final String PHONE_NUMBER = "phone_number";
    public static final String ASSIGNED_VILLAGES = "assigned_villages";
    public static final String REMINDER_DAYS = "reminder_days";
    public static final String REMINDER_HOUR = "reminder_hour";
    public static final String CLOSE_CASE = "close_case";
    public static final String JOB_ID = "JobID";
    public static final String GUARDIAN_NAME =  "guardian_name";
    public static final String CHILD_ID =  "child_id";
    public static final String VILLAGE_NAME = "village_name";
    public static final String DATE_MODIFIED = "date_modified";
    public static final String LAST_DOSE_DATE = "last_dose_date";
}
