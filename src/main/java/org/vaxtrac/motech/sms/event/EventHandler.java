package org.vaxtrac.motech.sms.event;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.motechproject.event.MotechEvent;
import org.motechproject.event.listener.annotations.MotechListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vaxtrac.motech.sms.domain.CommcareGuardianCase;
import org.vaxtrac.motech.sms.domain.CommcarePatientCase;
import org.vaxtrac.motech.sms.domain.Relais;
import org.vaxtrac.motech.sms.domain.RelaisVillage;
import org.vaxtrac.motech.sms.domain.SmsReminderLog;
import org.vaxtrac.motech.sms.parser.Vaccine;
import org.vaxtrac.motech.sms.parser.VaccineParserImpl;
import org.vaxtrac.motech.sms.service.CommcareGuardianService;
import org.vaxtrac.motech.sms.service.CommcarePatientService;
import org.vaxtrac.motech.sms.service.CommcareVillageMapperService;
import org.vaxtrac.motech.sms.service.RelaisLogService;
import org.vaxtrac.motech.sms.service.RelaisService;
import org.vaxtrac.motech.sms.service.SmsReminderLogService;
import org.vaxtrac.motech.sms.service.SmsSchedulerService;
import org.vaxtrac.motech.sms.service.WhitelistedClinicService;

/**
 * Event Handler for the VaxTrac module. The public methods listen for the event subjects listed in
 * {@link org.vaxtrac.motech.sms.event.EventSubjects}
 *
 */
@Service
public class EventHandler {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private final static String FINISHED = "finished";
	
	@Autowired
	private EventEmitter eventEmitter;

	@Autowired
	private CommcareVillageMapperService commcareVillageService;

	@Autowired
	private RelaisService relaisService;

	@Autowired
	private SmsSchedulerService smsSchedulerService;

	@Autowired
	private CommcareGuardianService guardianService;

	@Autowired
	private CommcarePatientService patientService;

	@Autowired
	private WhitelistedClinicService whitelistedClinicService;

	@Autowired
	private SmsReminderLogService smsReminderLogService;
	
	@Autowired
	private RelaisLogService relaisLogService;
	

	public EventHandler(
			EventEmitter eventEmitter,
			CommcareVillageMapperService commcareVillageService,
			RelaisService relaisService, SmsSchedulerService smsSchedulerService,
			CommcareGuardianService commcareGuardianService,
			CommcarePatientService patientService,
			WhitelistedClinicService whitelistedClinicService,
			RelaisLogService relaisLogService) {
		this.eventEmitter = eventEmitter;
		this.commcareVillageService = commcareVillageService;
		this.relaisService = relaisService;
		this.smsSchedulerService = smsSchedulerService;
		this.guardianService = commcareGuardianService;
		this.patientService = patientService;
		this.whitelistedClinicService = whitelistedClinicService;
		this.relaisLogService = relaisLogService;
	}

	public EventHandler() { }

	@MotechListener(subjects = {EventSubjects.TASK_MODULE_NOTIFICATION})
	public void handleTaskModuleNotification(MotechEvent event) {
		String message = "Received an event with subject " + event.getSubject();
		logger.info(message);

		Map<String, Object> params = new HashMap<String, Object>(event.getParameters());
		message = (String) params.remove(EventParams.MESSAGE);
		eventEmitter.emitDisabledTaskEvent(message);
	}


	@MotechListener(subjects = {EventSubjects.SCHEDULE_RELAIS_SMS})
	public void handleScheduleRelaisSms(MotechEvent event) {
		//schedule cron job
		logger.info("Received an event with subject " + event.getSubject());

		Map<String, Object> params = new HashMap<String, Object>(event.getParameters());
		String userId = (String) params.get(EventParams.USER_ID);

		//Only process relais whose clinics are whitelisted
		if (whitelistedClinicService.findByClinicId(userId) != null) {

			String caseId = (String) params.get(EventParams.CASE_ID);
			String closeCase = (String) params.get(EventParams.CLOSE_CASE);
			String relaisName = (String) params.get(EventParams.RELAIS_NAME);
			String phoneNumber = (String) params.get(EventParams.PHONE_NUMBER);
			
			Relais relais = relaisService.findByCaseId(caseId);

			if (!closeCase.equals("true")) {
				String assignedVillages = (String) params.get(EventParams.ASSIGNED_VILLAGES);
				String reminderDays = (String) params.get(EventParams.REMINDER_DAYS);
				String reminderHour = (String) params.get(EventParams.REMINDER_HOUR);
				String modificationDate = (String) params.get(EventParams.DATE_MODIFIED);

				//if relais unregistered, register relais and schedule new job
				if (relais == null) {
					logger.debug("New relais");
					relaisService.create(caseId, relaisName, phoneNumber, assignedVillages, 
							reminderDays, reminderHour, userId);
					smsSchedulerService.scheduleSendSmsJob(caseId, reminderDays, reminderHour);
					smsSchedulerService.sendSubscriptionSms(relaisName, phoneNumber);
				}
				//else update existing relais and/or job
				else {
					logger.debug("Updated relais");
					if (!reminderDays.equals(relais.getReminderDays()) || 
							!reminderHour.equals(relais.getReminderHour())) {
						smsSchedulerService.updateSendSmsJob(caseId, reminderDays, reminderHour);
					}
					if (!phoneNumber.equals(relais.getPhoneNumber())) {
						smsSchedulerService.sendPhoneUpdateSms(relaisName, phoneNumber);
					}
					
					//log current value of modified fields
					
					if (!relais.getName().equals(relaisName))
						relaisLogService.create(caseId, userId, modificationDate, EventParams.RELAIS_NAME, relais.getName());
					
					if (!relais.getPhoneNumber().equals(phoneNumber))
						relaisLogService.create(caseId, userId, modificationDate, EventParams.PHONE_NUMBER, relais.getPhoneNumber());
					
					if (!relais.getReminderDays().equals(reminderDays))
						relaisLogService.create(caseId, userId, modificationDate, EventParams.REMINDER_DAYS, relais.getReminderDays());
					
					if (!relais.getReminderHour().equals(reminderHour))
						relaisLogService.create(caseId, userId, modificationDate, EventParams.REMINDER_HOUR, relais.getReminderHour());
					
					List<RelaisVillage> villageList = relaisService.parseVillages(assignedVillages, caseId);
					if (!relaisService.isSameVillageList(villageList, relais.getVillages())) {
						for (RelaisVillage village : relais.getVillages()) {
							relaisLogService.create(caseId, userId, modificationDate, EventParams.VILLAGE, village.getVillageId());
						}
					}

					relaisService.update(caseId, relaisName, phoneNumber, assignedVillages, reminderDays, reminderHour, userId);
				}
			}

			//close case
			else {
				//stop job, delete relais
				logger.debug("Deleted relais");
				if (relais != null) {
					smsSchedulerService.unscheduleSendSmsJob(caseId);
					smsSchedulerService.sendUnsubscriptionSms(relaisName, phoneNumber);
					relaisService.delete(relais);
				}
			}    
		}
	}
	
	
	@MotechListener(subjects = {EventSubjects.SEND_RELAIS_SMS})
	public void handleSendRelaisSms(MotechEvent event) {
		//format and send sms
		logger.info("Received an event with subject " + event.getSubject());
		Map<String, Object> params = new HashMap<String, Object>(event.getParameters());
		try {
			smsSchedulerService.sendReminderSms((String) params.get(EventParams.CASE_ID));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@MotechListener(subjects = {EventSubjects.LOG_PATIENT})
	public void handleLogPatient(MotechEvent event) {
		Map<String, Object> params = new HashMap<String, Object>(event.getParameters());

		String userId = (String) params.get(EventParams.USER_ID);

		//Only process patients whose clinics are whitelisted
		if (whitelistedClinicService.findByClinicId(userId) != null) {

			String caseId = (String) params.get(EventParams.CASE_ID);
			String patientName = ((String) params.get(EventParams.PATIENT_NAME)).trim().replaceAll("\\s+", " ");
			String villageName = (String) params.get(EventParams.VILLAGE_NAME);
			String callbackDate = (String) params.get(EventParams.CALLBACK_DATE);
			String contactNumber = (String) params.get(EventParams.CONTACT_NUMBER);
			String dateModified = (String) params.get(EventParams.DATE_MODIFIED);
			String recordedDoses = (String) params.get(EventParams.RECORDED_VACCINES);

			CommcarePatientCase patient = patientService.findByCaseId(caseId);

			VaccineParserImpl parser = new VaccineParserImpl();
			Vaccine lastDose = parser.findMostRecentDose(recordedDoses);
			DateTime lastDoseDate;
			if (lastDose != null)
				lastDoseDate = lastDose.getDate();
			else
				lastDoseDate = null;

			if (!callbackDate.equals(FINISHED)) {
				if (isWellFormated(callbackDate)) {
					if (patient == null) {
						logger.info("create patient: " + caseId);
						patientService.create(caseId, userId, patientName, contactNumber, villageName, callbackDate, lastDoseDate);
					}

					else {
						if (patient.callbackDateIsBeforeOrEqual(callbackDate)) {
							logger.info("update patient: " + caseId);

							updateReturnDate(lastDoseDate, patient);

							patient.setPatientName(patientName);
							patient.setCallbackDate(callbackDate);
							patient.setContactNumber(contactNumber);
							patient.setuserId(userId);
							patient.setVillageName(villageName);
							patient.setLastDoseDate(lastDose.getDate());

							patientService.update(patient);
						}
					}
				}
			}

			else {
				if (patient != null) {
					logger.info("delete patient: " + caseId);
					updateReturnDate(lastDoseDate, patient);
					patientService.delete(patient);

					List<CommcareGuardianCase> guardians = guardianService.findByChildId(caseId);
					Iterator<CommcareGuardianCase> it = guardians.iterator();
					while (it.hasNext()) {
						CommcareGuardianCase guardian = it.next();
						logger.info("delete guardian: " + guardian.getCaseId());
						guardianService.delete(guardian);
					}
				}
				logger.info("patient finished: " + caseId);
			}
		}
	}

	boolean isWellFormated(String date) {
		DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
		try {
			formatter.parseDateTime(date);
			return true;
		}
		catch (Exception e) {
			return false;
		}
	}

	void updateReturnDate(DateTime lastDoseDate, CommcarePatientCase patient) {
		if (lastDoseDate != null && patient.getLastDoseDate() != null && lastDoseDate.isAfter(patient.getLastDoseDate())
				|| lastDoseDate != null && patient.getLastDoseDate() == null) {
		
			List<SmsReminderLog> reminderLogs = smsReminderLogService.findByPatientAndCallbackDate(patient.getCaseId(), patient.getCallbackDate());
			logger.info(reminderLogs.size() + " logs");
			
			Iterator<SmsReminderLog> it = reminderLogs.iterator();
			while (it.hasNext()) {
				SmsReminderLog log = it.next();
				if (log.getReturnDate() == null || log.getReturnDate().equals("")) {
					logger.info(patient.getCaseId() + " returned on " + lastDoseDate + " - " + log.getType());
					log.setReturnDate(lastDoseDate);
					smsReminderLogService.update(log);
				}
			}
		}
	}


	@MotechListener(subjects = {EventSubjects.LOG_GUARDIAN})
	public void handleLogGuardian(MotechEvent event) {
		Map<String, Object> params = new HashMap<String, Object>(event.getParameters());

		String userId = (String) params.get(EventParams.USER_ID);

		//Only process guardians whose clinics are whitelisted
		if (whitelistedClinicService.findByClinicId(userId) != null) {
			String caseId = (String) params.get(EventParams.CASE_ID);
			String guardianName = ((String) params.get(EventParams.GUARDIAN_NAME)).trim().replaceAll("\\s+", " ");
			String childId = (String) params.get(EventParams.CHILD_ID);

			CommcareGuardianCase guardian = guardianService.findByCaseId(caseId);

			if (guardian == null) {
				guardianService.create(caseId, userId, guardianName, childId);
			}
			else {
				guardian.setGuardianName(guardianName);
				guardian.setUserId(userId);
				guardian.setChildId(childId);
				guardianService.update(guardian);
			}
		}
	}
}
