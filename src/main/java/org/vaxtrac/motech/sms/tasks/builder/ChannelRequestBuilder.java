package org.vaxtrac.motech.sms.tasks.builder;

import java.util.ArrayList;
import java.util.List;

import org.motechproject.tasks.contract.ActionEventRequest;
import org.motechproject.tasks.contract.ChannelRequest;
import org.motechproject.tasks.contract.TriggerEventRequest;
import org.osgi.framework.BundleContext;
import org.vaxtrac.motech.sms.tasks.DisplayNames;

/**
 * Builds a channel request from the records in MDS pertaining to the VaxTrac instance schema.
 *OB
 */

public class ChannelRequestBuilder {

    private BundleContext bundleContext;
	
    public ChannelRequestBuilder(BundleContext bundleContext){
	this.bundleContext = bundleContext;
    }

    /**
     * Creates task action event requests for vaccination records checks.
     * @return the new Channel Request
     */
    public ChannelRequest build() {
	ScheduleRelaisSmsActionBuilder scheduleRelaisSmsActionBuilder = new ScheduleRelaisSmsActionBuilder();
	LogPatientActionBuilder logPatientActionBuilder = new LogPatientActionBuilder();
	LogGuardianActionBuilder logGuardianActionBuilder = new LogGuardianActionBuilder();
	
	List<ActionEventRequest> actions = new ArrayList<ActionEventRequest>();
	actions.add(scheduleRelaisSmsActionBuilder.build());
	actions.add(logGuardianActionBuilder.build());
	actions.add(logPatientActionBuilder.build());
	
	DisabledTaskTriggerBuilder disabledTaskTriggerBuilder = new DisabledTaskTriggerBuilder();

	List<TriggerEventRequest> triggers = new ArrayList<TriggerEventRequest>();
	triggers.add(disabledTaskTriggerBuilder.buildTrigger());
		
	return new ChannelRequest(DisplayNames.VAXTRAC_DISPLAY_NAME, bundleContext.getBundle().getSymbolicName(),
				  bundleContext.getBundle().getVersion().toString(), null, triggers, actions);
    }
}
