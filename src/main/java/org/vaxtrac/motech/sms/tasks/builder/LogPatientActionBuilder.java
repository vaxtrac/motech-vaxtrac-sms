package org.vaxtrac.motech.sms.tasks.builder;

import java.util.SortedSet;
import java.util.TreeSet;

import org.motechproject.tasks.contract.ActionEventRequest;
import org.motechproject.tasks.contract.ActionEventRequestBuilder;
import org.motechproject.tasks.contract.ActionParameterRequest;
import org.motechproject.tasks.contract.ActionParameterRequestBuilder;
import org.vaxtrac.motech.sms.event.EventParams;
import org.vaxtrac.motech.sms.event.EventSubjects;
import org.vaxtrac.motech.sms.tasks.DisplayNames;

public class LogPatientActionBuilder {
	public ActionEventRequest build() {
		SortedSet<ActionParameterRequest> actionParameters = new TreeSet<ActionParameterRequest>();

		int counter = 0;

		ActionParameterRequestBuilder actionParameterBuilder = new ActionParameterRequestBuilder()
		.setDisplayName(DisplayNames.CASE_ID)
		.setKey(EventParams.CASE_ID)
		.setRequired(true)
		.setOrder(counter++);

		actionParameters.add(actionParameterBuilder.createActionParameterRequest());
		
		actionParameterBuilder = new ActionParameterRequestBuilder()
		.setDisplayName(DisplayNames.PATIENT_NAME)
		.setKey(EventParams.PATIENT_NAME)
		.setRequired(true)
		.setOrder(counter++);

		actionParameters.add(actionParameterBuilder.createActionParameterRequest());
		
		actionParameterBuilder = new ActionParameterRequestBuilder()
		.setDisplayName(DisplayNames.USER_ID)
		.setKey(EventParams.USER_ID)
		.setRequired(true)
		.setOrder(counter++);

		actionParameters.add(actionParameterBuilder.createActionParameterRequest());

		actionParameterBuilder = new ActionParameterRequestBuilder()
		.setDisplayName(DisplayNames.VILLAGE_NAME)
		.setKey(EventParams.VILLAGE_NAME)
		.setRequired(true)
		.setOrder(counter++);

		actionParameters.add(actionParameterBuilder.createActionParameterRequest());

		actionParameterBuilder = new ActionParameterRequestBuilder()
		.setDisplayName(DisplayNames.CALLBACK_DATE)
		.setKey(EventParams.CALLBACK_DATE)
		.setRequired(true)
		.setOrder(counter++);

		actionParameters.add(actionParameterBuilder.createActionParameterRequest());
		
		actionParameterBuilder = new ActionParameterRequestBuilder()
		.setDisplayName(DisplayNames.CONTACT_NUMBER)
		.setKey(EventParams.CONTACT_NUMBER)
		.setRequired(true)
		.setOrder(counter++);

		actionParameters.add(actionParameterBuilder.createActionParameterRequest());
		
		actionParameterBuilder = new ActionParameterRequestBuilder()
		.setDisplayName(DisplayNames.DATE_MODIFIED)
		.setKey(EventParams.DATE_MODIFIED)
		.setRequired(true)
		.setOrder(counter++);

		actionParameters.add(actionParameterBuilder.createActionParameterRequest());
		
		actionParameterBuilder = new ActionParameterRequestBuilder()
		.setDisplayName(DisplayNames.RECORDED_VACCINES)
		.setKey(EventParams.RECORDED_VACCINES)
		.setRequired(true)
		.setOrder(counter++);

		actionParameters.add(actionParameterBuilder.createActionParameterRequest());

		ActionEventRequestBuilder actionEventBuilder = new ActionEventRequestBuilder()
		.setActionParameters(actionParameters)
		.setDisplayName(DisplayNames.LOG_PATIENT)
		.setSubject(EventSubjects.LOG_PATIENT);

		return actionEventBuilder.createActionEventRequest();
	}
}
