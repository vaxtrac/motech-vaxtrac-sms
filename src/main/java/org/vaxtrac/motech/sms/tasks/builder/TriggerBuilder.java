package org.vaxtrac.motech.sms.tasks.builder;

import org.motechproject.tasks.contract.TriggerEventRequest;
import java.util.List;

public interface TriggerBuilder {
	TriggerEventRequest buildTrigger();
}
