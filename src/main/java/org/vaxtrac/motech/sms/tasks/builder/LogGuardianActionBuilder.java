package org.vaxtrac.motech.sms.tasks.builder;

import java.util.SortedSet;
import java.util.TreeSet;

import org.motechproject.tasks.contract.ActionEventRequest;
import org.motechproject.tasks.contract.ActionEventRequestBuilder;
import org.motechproject.tasks.contract.ActionParameterRequest;
import org.motechproject.tasks.contract.ActionParameterRequestBuilder;
import org.vaxtrac.motech.sms.event.EventParams;
import org.vaxtrac.motech.sms.event.EventSubjects;
import org.vaxtrac.motech.sms.tasks.DisplayNames;

public class LogGuardianActionBuilder {
	public ActionEventRequest build() {
		SortedSet<ActionParameterRequest> actionParameters = new TreeSet<ActionParameterRequest>();

		int counter = 0;

		ActionParameterRequestBuilder actionParameterBuilder = new ActionParameterRequestBuilder()
		.setDisplayName(DisplayNames.CASE_ID)
		.setKey(EventParams.CASE_ID)
		.setRequired(true)
		.setOrder(counter++);

		actionParameters.add(actionParameterBuilder.createActionParameterRequest());
		
		actionParameterBuilder = new ActionParameterRequestBuilder()
		.setDisplayName(DisplayNames.USER_ID)
		.setKey(EventParams.USER_ID)
		.setRequired(true)
		.setOrder(counter++);

		actionParameters.add(actionParameterBuilder.createActionParameterRequest());
		
		actionParameterBuilder = new ActionParameterRequestBuilder()
		.setDisplayName(DisplayNames.GUARDIAN_NAME)
		.setKey(EventParams.GUARDIAN_NAME)
		.setRequired(true)
		.setOrder(counter++);

		actionParameters.add(actionParameterBuilder.createActionParameterRequest());
		
		actionParameterBuilder = new ActionParameterRequestBuilder()
		.setDisplayName(DisplayNames.CHILD_ID)
		.setKey(EventParams.CHILD_ID)
		.setRequired(true)
		.setOrder(counter++);

		actionParameters.add(actionParameterBuilder.createActionParameterRequest());

		ActionEventRequestBuilder actionEventBuilder = new ActionEventRequestBuilder()
		.setActionParameters(actionParameters)
		.setDisplayName(DisplayNames.LOG_GUARDIAN)
		.setSubject(EventSubjects.LOG_GUARDIAN);

		return actionEventBuilder.createActionEventRequest();
	}
}
