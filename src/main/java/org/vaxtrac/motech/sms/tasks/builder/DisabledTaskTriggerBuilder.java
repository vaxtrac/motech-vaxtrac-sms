package org.vaxtrac.motech.sms.tasks.builder;

import java.util.ArrayList;
import java.util.List;

import org.motechproject.tasks.contract.EventParameterRequest;
import org.motechproject.tasks.contract.TriggerEventRequest;
import org.vaxtrac.motech.sms.event.EventParams;
import org.vaxtrac.motech.sms.event.EventSubjects;
import org.vaxtrac.motech.sms.tasks.DisplayNames;

public class DisabledTaskTriggerBuilder implements TriggerBuilder{

    @Override
    public TriggerEventRequest buildTrigger() {
	List<EventParameterRequest> parameterRequests = new ArrayList<EventParameterRequest>();

	parameterRequests.add(new EventParameterRequest(DisplayNames.MESSAGE, EventParams.MESSAGE));

	TriggerEventRequest trigger = new TriggerEventRequest(
							      DisplayNames.TASK_DISABLED, // display name
							      EventSubjects.TASK_DISABLED, // subject
							      null, // description
							      parameterRequests // event parameters
							      );

	return trigger;
    }

}
