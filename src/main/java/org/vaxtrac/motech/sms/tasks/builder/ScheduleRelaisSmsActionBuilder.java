package org.vaxtrac.motech.sms.tasks.builder;

import org.motechproject.tasks.contract.ActionEventRequest;
import org.motechproject.tasks.contract.ActionEventRequestBuilder;
import org.motechproject.tasks.contract.ActionParameterRequest;
import org.motechproject.tasks.contract.ActionParameterRequestBuilder;
import org.vaxtrac.motech.sms.event.EventParams;
import org.vaxtrac.motech.sms.event.EventSubjects;
import org.vaxtrac.motech.sms.tasks.DisplayNames;

import java.util.SortedSet;
import java.util.TreeSet;


public class ScheduleRelaisSmsActionBuilder {
    public ActionEventRequest build() {
		SortedSet<ActionParameterRequest> actionParameters = new TreeSet<ActionParameterRequest>();

		int counter = 0;

		ActionParameterRequestBuilder actionParameterBuilder = new ActionParameterRequestBuilder()
		.setDisplayName(DisplayNames.CASE_ID)
		.setKey(EventParams.CASE_ID)
		.setRequired(true)
		.setOrder(counter++);

		actionParameters.add(actionParameterBuilder.createActionParameterRequest());
		
		actionParameterBuilder = new ActionParameterRequestBuilder()
		.setDisplayName(DisplayNames.RELAIS_NAME)
		.setKey(EventParams.RELAIS_NAME)
		.setRequired(true)
		.setOrder(counter++);

		actionParameters.add(actionParameterBuilder.createActionParameterRequest());
		
		actionParameterBuilder = new ActionParameterRequestBuilder()
		.setDisplayName(DisplayNames.PHONE_NUMBER)
		.setKey(EventParams.PHONE_NUMBER)
		.setRequired(true)
		.setOrder(counter++);

		actionParameters.add(actionParameterBuilder.createActionParameterRequest());
		
		actionParameterBuilder = new ActionParameterRequestBuilder()
		.setDisplayName(DisplayNames.ASSIGNED_VILLAGES)
		.setKey(EventParams.ASSIGNED_VILLAGES)
		.setRequired(true)
		.setOrder(counter++);

		actionParameters.add(actionParameterBuilder.createActionParameterRequest());
		
		actionParameterBuilder = new ActionParameterRequestBuilder()
		.setDisplayName(DisplayNames.REMINDER_DAYS)
		.setKey(EventParams.REMINDER_DAYS)
		.setRequired(true)
		.setOrder(counter++);

		actionParameters.add(actionParameterBuilder.createActionParameterRequest());
		
		actionParameterBuilder = new ActionParameterRequestBuilder()
		.setDisplayName(DisplayNames.REMINDER_HOUR)
		.setKey(EventParams.REMINDER_HOUR)
		.setRequired(true)
		.setOrder(counter++);

		actionParameters.add(actionParameterBuilder.createActionParameterRequest());
		
		actionParameterBuilder = new ActionParameterRequestBuilder()
		.setDisplayName(DisplayNames.CLOSE_CASE)
		.setKey(EventParams.CLOSE_CASE)
		.setRequired(true)
		.setOrder(counter++);

		actionParameters.add(actionParameterBuilder.createActionParameterRequest());
		
		actionParameterBuilder = new ActionParameterRequestBuilder()
		.setDisplayName(DisplayNames.USER_ID)
		.setKey(EventParams.USER_ID)
		.setRequired(true)
		.setOrder(counter++);

		actionParameters.add(actionParameterBuilder.createActionParameterRequest());
		
		actionParameterBuilder = new ActionParameterRequestBuilder()
		.setDisplayName(DisplayNames.DATE_MODIFIED)
		.setKey(EventParams.DATE_MODIFIED)
		.setRequired(true)
		.setOrder(counter++);

		actionParameters.add(actionParameterBuilder.createActionParameterRequest());

		
		ActionEventRequestBuilder actionEventBuilder = new ActionEventRequestBuilder()
		.setActionParameters(actionParameters)
		.setDisplayName(DisplayNames.SCHEDULE_RELAIS_SMS)
		.setSubject(EventSubjects.SCHEDULE_RELAIS_SMS);

		return actionEventBuilder.createActionEventRequest();
	}
}
