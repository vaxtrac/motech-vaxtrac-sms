package org.vaxtrac.motech.sms.domain;

import org.motechproject.mds.annotations.Entity;
import org.motechproject.mds.annotations.Field;

@Entity
public class RelaisLog {
	@Field
	private String relaisId;
	
	@Field
	private String userId;
	
	@Field
	private String modificationDate;
	
	@Field
	private String field;
	
	@Field
	private String oldValue;

	public RelaisLog(String relaisId, String userId, String modificationDate,
			String field, String oldValue) {
		this.relaisId = relaisId;
		this.userId = userId;
		this.modificationDate = modificationDate;
		this.field = field;
		this.oldValue = oldValue;
	}
	
	public RelaisLog() {}
	
	public String getRelaisId() {
		return relaisId;
	}

	public void setRelaisId(String relaisId) {
		this.relaisId = relaisId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(String modificationDate) {
		this.modificationDate = modificationDate;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getOldValue() {
		return oldValue;
	}

	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RelaisLog other = (RelaisLog) obj;
		if (field == null) {
			if (other.field != null)
				return false;
		} else if (!field.equals(other.field))
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (oldValue == null) {
			if (other.oldValue != null)
				return false;
		} else if (!oldValue.equals(other.oldValue))
			return false;
		if (relaisId == null) {
			if (other.relaisId != null)
				return false;
		} else if (!relaisId.equals(other.relaisId))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}
	
	
}
