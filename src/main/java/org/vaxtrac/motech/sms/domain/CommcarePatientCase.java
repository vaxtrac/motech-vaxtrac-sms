package org.vaxtrac.motech.sms.domain;

import java.util.Objects;

import javax.jdo.annotations.Unique;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.motechproject.mds.annotations.Entity;
import org.motechproject.mds.annotations.Field;
import org.motechproject.mds.annotations.Ignore;

@Entity
public class CommcarePatientCase implements Comparable<CommcarePatientCase> {
	@Field
	@Unique
	private String caseId;

	@Field
	private String userId;

	@Field
	private String patientName;

	@Field
	private String villageName;

	@Field
	private DateTime callbackDate;

	@Field
	private String contactNumber;
	
	@Field
	private DateTime lastDoseDate;

	@Ignore
	private boolean missed = false;


	private DateTimeFormatter formatter;
	private final String DATE_PATTERN = "yyyy-MM-dd";

	@Override
	public String toString() {
		if (missed) {
			return "Pat: " + patientName + "\nVil: " + villageName + "\nRap: "
					+ formatter.print(callbackDate) + " M\nCont: " + contactNumber;
		}
		else {
			return "Pat: " + patientName + "\nVil: " + villageName + "\nRap: "
					+ formatter.print(callbackDate) + "\nCont: " + contactNumber;
		}
	}

	private void init(String caseId, String userId,
			String patientName, String contactNumber, String villageName) {
		formatter = DateTimeFormat.forPattern(DATE_PATTERN);
		this.caseId = caseId;
		this.userId = userId;
		this.patientName = patientName;
		this.contactNumber = contactNumber;
		this.villageName = villageName;
	}

	public CommcarePatientCase() {
		formatter = DateTimeFormat.forPattern(DATE_PATTERN);
	}

	public CommcarePatientCase(String caseId, String userId,
			String patientName, String contactNumber, String villageName,
			String callbackDate, DateTime lastDoseDate) {
		init(caseId, userId,patientName, contactNumber, villageName);
		this.callbackDate = parseDate(callbackDate);
		this.lastDoseDate = lastDoseDate;
	}

	public CommcarePatientCase(String caseId, String userId,
			String patientName, String contactNumber, String villageName,
			DateTime callbackDate, DateTime lastDoseDate) {
		init(caseId, userId,patientName, contactNumber, villageName);
		this.callbackDate = callbackDate;
		this.lastDoseDate = lastDoseDate;
	}

	private DateTime parseDate(String date) {
		try {
			return formatter.parseDateTime(date);
		}
		catch (Exception e) {
			return DateTime.now();
		}
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		if (caseId != null && !caseId.equals(""))
			this.caseId = caseId;
	}

	public String getuserId() {
		return userId;
	}

	public void setuserId(String userId) {
		if (userId != null && !userId.equals(""))
			this.userId = userId;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		if (patientName != null && !patientName.equals(""))
			this.patientName = patientName;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		if (contactNumber != null && !contactNumber.equals(""))
			this.contactNumber = contactNumber;
	}

	public String getVillageName() {
		return villageName;
	}

	public void setVillageName(String villageName) {
		if (villageName != null && !villageName.equals(""))
			this.villageName = villageName;
	}

	public DateTime getCallbackDate() {
		return callbackDate;
	}

	public String callbackDateString() {
		return callbackDate.toString(DATE_PATTERN);
	}

	public void setCallbackDate (DateTime callbackDate) {
		if (callbackDate != null)
			this.callbackDate = callbackDate;
	}

	public void setCallbackDate(String date) {
		if (date != null)
			parseDate(date);
	}
	
	boolean isWellFormated(String date) {
		try {
			formatter.parseDateTime(date);
			return true;
		}
		catch (Exception e) {
			return false;
		}
	}

	public boolean callbackDateIsBefore(String date) {
		if (isWellFormated(date))
			return callbackDate.isBefore(formatter.parseDateTime(date));
		else
			return false;
	}
	
	public boolean callbackDateIsBeforeOrEqual(String date) {
		if (isWellFormated(date))
			return !callbackDate.isAfter(formatter.parseDateTime(date));
		else
			return false;
	}

	public boolean isMissed() {
		return missed;
	}

	public void setMissed(boolean missed) {
		this.missed = missed;
	}

	public DateTime getLastDoseDate() {
		return lastDoseDate;
	}

	public void setLastDoseDate(DateTime lastDoseDate) {
		this.lastDoseDate = lastDoseDate;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		final CommcarePatientCase other = (CommcarePatientCase) obj;
		return Objects.equals(this.caseId, other.caseId);
	}

	@Override
	public int compareTo(CommcarePatientCase other) {
		return this.callbackDate.compareTo(other.callbackDate);
	}

	
}
