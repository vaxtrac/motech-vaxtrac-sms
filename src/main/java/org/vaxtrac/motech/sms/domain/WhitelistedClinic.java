package org.vaxtrac.motech.sms.domain;

import java.util.Objects;

import javax.jdo.annotations.Unique;

import org.motechproject.mds.annotations.Entity;
import org.motechproject.mds.annotations.Field;

@Entity
public class WhitelistedClinic {
	@Field
	@Unique
	private String clinicId;
	
	@Field
	private String name;

	public WhitelistedClinic(String clinicId, String name) {
		this.clinicId = clinicId;
		this.name = name;
	}

	public String getClinicId() {
		return clinicId;
	}

	public void setClinicId(String clinicId) {
		this.clinicId = clinicId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		final WhitelistedClinic other = (WhitelistedClinic) obj;
		return Objects.equals(this.clinicId, other.clinicId);
	}
}
