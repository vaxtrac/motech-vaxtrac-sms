package org.vaxtrac.motech.sms.domain;

import org.motechproject.mds.annotations.Entity;
import org.motechproject.mds.annotations.Field;

import javax.jdo.annotations.Unique;

import java.util.Objects;

@Entity
public class CommcareGuardianCase {
	@Field
	@Unique
	private String caseId;

	@Field
	private String userId;

	@Field
	private String guardianName;

	@Field
	private String childId;

	public CommcareGuardianCase() {}

	public CommcareGuardianCase(String caseId, String userId,
			String guardianName, String childId) {
		this.caseId = caseId;
		this.userId = userId;
		this.guardianName = guardianName;
		this.childId = childId;
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		if (caseId != null && !caseId.equals(""))
			this.caseId = caseId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		if (userId != null && !userId.equals(""))
			this.userId = userId;
	}

	public String getGuardianName() {
		return guardianName;
	}

	public void setGuardianName(String guardianName) {
		if (guardianName != null && !guardianName.equals(""))
			this.guardianName = guardianName;
	}

	public String getChildId() {
		return childId;
	}

	public void setChildId(String childId) {
		if (childId != null && !childId.equals(""))
			this.childId = childId;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		final CommcareGuardianCase other = (CommcareGuardianCase) obj;
		return Objects.equals(this.caseId, other.caseId);
	}
}
