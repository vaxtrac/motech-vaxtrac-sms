package org.vaxtrac.motech.sms.domain;

import java.util.Objects;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.motechproject.mds.annotations.Entity;
import org.motechproject.mds.annotations.Field;

@Entity
public class SmsReminderLog {
	@Field
	private String relaisId;
	
	@Field
	private String patientId;
	
	@Field
	private String clinicId;
	
	@Field
	private DateTime callbackDate;
	
	@Field
	private DateTime reminderDate;
	
	@Field
	private String returnDate;
	
	@Field
	private String type;
	
	private DateTimeFormatter formatter;
	private final String DATE_PATTERN = "yyyy-MM-dd";

	public SmsReminderLog(String relaisId, String patientId,
			DateTime dateTime, DateTime reminderDate, String type, String clinicId) {
		formatter = DateTimeFormat.forPattern(DATE_PATTERN);
		this.relaisId = relaisId;
		this.patientId = patientId;
		this.callbackDate = dateTime;
		this.reminderDate = reminderDate;
		this.returnDate = null;
		this.type = type;
		this.clinicId = clinicId;
	}
	
	public SmsReminderLog() {}

	public String getRelaisId() {
		return relaisId;
	}

	public void setRelaisId(String relaisId) {
		this.relaisId = relaisId;
	}
	
	public String getClinicId() {
		return clinicId;
	}

	public void setClinicId(String clinicId) {
		this.clinicId = clinicId;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public DateTime getCallbackDate() {
		return callbackDate;
	}

	public void setCallbackDate(DateTime callbackDate) {
		this.callbackDate = callbackDate;
	}

	
	public DateTime getReminderDate() {
		return reminderDate;
	}

	public void setReminderDate(DateTime reminderDate) {
		this.reminderDate = reminderDate;
	}
	
	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}
	
	public void setReturnDate(DateTime returnDate) {
		this.returnDate = returnDate.toString(formatter);
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		final SmsReminderLog other = (SmsReminderLog) obj;
		return Objects.equals(this.relaisId, other.relaisId)
				&& Objects.equals(this.patientId, other.patientId)
				&& Objects.equals(this.reminderDate, other.reminderDate)
				&& Objects.equals(this.callbackDate, other.callbackDate)
				&& Objects.equals(this.type, other.type);
	}
}
