package org.vaxtrac.motech.sms.domain;

import org.motechproject.mds.annotations.Entity;
import org.motechproject.mds.annotations.Field;
import org.vaxtrac.motech.sms.domain.RelaisVillage;

import javax.jdo.annotations.Unique;

import java.util.Iterator;
import java.util.Objects;
import java.util.List;

/**
 * Models data for simple records in a portable manner.
 */
@Entity
public class Relais {

	@Field
	@Unique
	private String caseId;

	@Field
	private String name;

	@Field
	private String phoneNumber;

	@Field
	private List<RelaisVillage> villages;

	@Field
	private String reminderDays;

	@Field
	private String reminderHour;

	@Field
	private String clinicId;


	public Relais(String caseId, String name, String phoneNumber,
			List<RelaisVillage> villages, String reminderDays, String reminderHour,
			String clinicId) {
		this.caseId = caseId;
		this.name = name;
		this.phoneNumber = phoneNumber;
		this.villages = villages;
		this.reminderDays = reminderDays;
		this.reminderHour = reminderHour;
		this.clinicId = clinicId;
	}

	public Relais() {}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		if (caseId != null && !caseId.equals(""))
			this.caseId = caseId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name != null && !name.equals(""))
			this.name = name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		if (phoneNumber != null && !phoneNumber.equals(""))
			this.phoneNumber = phoneNumber;
	}

	public List<RelaisVillage> getVillages() {
		return villages;
	}

	public void setVillages(List<RelaisVillage> villages) {
		if (villages != null && !villages.isEmpty())
			this.villages = villages;
	}
	
	public String getVillagesAsString() {
		StringBuilder builder = new StringBuilder();
		Iterator<RelaisVillage> it = villages.iterator();
		while (it.hasNext()) {
			builder.append(it.next().getVillageId());
			builder.append(" ");
		}
		builder.setLength(builder.length()-1);
		return builder.toString();
	}

	public String getReminderDays() {
		return reminderDays;
	}

	public void setReminderDays(String reminderDays) {
		if (reminderDays != null && !reminderDays.equals(""))
			this.reminderDays = reminderDays;
	}

	public String getReminderHour() {
		return reminderHour;
	}

	public void setReminderHour(String reminderHour) {
		if (reminderHour != null && !reminderHour.equals(""))
			this.reminderHour = reminderHour;
	}

	public String getClinicId() {
		return clinicId;
	}

	public void setClinicId(String clinicId) {
		if (clinicId != null && !clinicId.equals(""))
			this.clinicId = clinicId;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		final Relais other = (Relais) obj;
		return Objects.equals(this.caseId, other.caseId);
	}
}
