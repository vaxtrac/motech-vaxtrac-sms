package org.vaxtrac.motech.sms.domain;

import java.util.Objects;

import org.motechproject.mds.annotations.Entity;
import org.motechproject.mds.annotations.Field;

@Entity
public class RelaisVillage {
	@Field
	String villageId;
	
	@Field
	String relaisId;

	public RelaisVillage(String villageId, String relaisId) {
		this.villageId = villageId;
		this.relaisId = relaisId;
	}

	public String getVillageId() {
		return villageId;
	}

	public void setVillageId(String villageId) {
		this.villageId = villageId;
	}

	public String getRelaisId() {
		return relaisId;
	}

	public void setRelaisId(String relaisId) {
		this.relaisId = relaisId;
	}

	@Override
    public boolean equals(Object obj) {
	if (obj == null || getClass() != obj.getClass()) {
	    return false;
	}

	final RelaisVillage other = (RelaisVillage) obj;
	return Objects.equals(this.villageId, other.villageId)
			&& Objects.equals(this.relaisId, other.relaisId);
    }
	
}
