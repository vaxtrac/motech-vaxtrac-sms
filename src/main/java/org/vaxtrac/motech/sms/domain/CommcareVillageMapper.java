package org.vaxtrac.motech.sms.domain;

import org.motechproject.mds.annotations.Entity;
import org.motechproject.mds.annotations.Field;
import javax.jdo.annotations.Unique;

import java.util.Objects;

/**
 * Models data for simple records in a portable manner.
 */
@Entity
public class CommcareVillageMapper {
    @Field
    private String villageId;

    @Field
    private String villageName;

    @Field
    private String userId;

    public CommcareVillageMapper() {
    }

    public CommcareVillageMapper(String villageId, String villageName,
				 String userId) {
	this.villageName = villageName;
	this.villageId = villageId;
	this.userId = userId;
    }

    public String getVillageId() {
	return villageId;
    }

    public void setVillageId(String string) {
	this.villageId = string;
    }
    
    public void setVillageName(String string) {
	this.villageName = string;
    }
    
    public String getVillageName() {
	return villageName;
    }

    public void setUserId(String string) {
	this.userId = string;
    }
    
    public String getUserId() {
	return userId;
    }


        @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}

	if (obj == null || getClass() != obj.getClass()) {
	    return false;
	}

	final CommcareVillageMapper other = (CommcareVillageMapper) obj;

	return Objects.equals(this.villageId, other.villageId);
    }
}
