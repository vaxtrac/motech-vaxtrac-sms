package org.vaxtrac.motech.sms.service;

import org.vaxtrac.motech.sms.domain.RelaisVillage;

import java.util.List;

public interface RelaisVillageService {
	public List<RelaisVillage> findAll();
    public List<RelaisVillage> findByRelaisId(String relaisId);
    public RelaisVillage findVillage(String villageId, String relaisId);
    public RelaisVillage create(String villageId, String relaisId);
    public void update(RelaisVillage relaisVillage);
    public void delete(RelaisVillage relaisVillage);
    public void deleteAll();
    public void add(RelaisVillage relaisVillage);
}
