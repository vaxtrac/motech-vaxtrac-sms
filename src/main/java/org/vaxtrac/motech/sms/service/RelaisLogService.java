package org.vaxtrac.motech.sms.service;

import java.util.List;

import org.vaxtrac.motech.sms.domain.RelaisLog;

public interface RelaisLogService {
	List<RelaisLog> findByRelaisId(String relaisId);
	List<RelaisLog> findByUserId(String userId);
	public List<RelaisLog> findAll();
    public RelaisLog create(RelaisLog relaisLog);
    public RelaisLog create(String relaisId, String userId, String modificationDate,
			String field, String oldValue);
    public void update(RelaisLog relaisLog);
    public void delete(RelaisLog relaisLog);
    public void deleteAll();
    public void add(RelaisLog relaisLog);
}
