package org.vaxtrac.motech.sms.service;

public interface ComputeSmsReminderDateService {
    String computeReminderDate(String callbackDate, int offset, int hour, int minutes, String timezoneId);
}
