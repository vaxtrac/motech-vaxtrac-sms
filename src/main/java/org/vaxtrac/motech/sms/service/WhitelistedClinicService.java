package org.vaxtrac.motech.sms.service;

import java.util.List;

import org.vaxtrac.motech.sms.domain.WhitelistedClinic;

public interface WhitelistedClinicService {
	WhitelistedClinic findByClinicId(String clinicId);
	public List<WhitelistedClinic> findAll();
    public WhitelistedClinic create(WhitelistedClinic whitelistedClinic);
    public void update(WhitelistedClinic whitelistedClinic);
    public void delete(WhitelistedClinic whitelistedClinic);
    public void deleteAll();
    public void add(WhitelistedClinic whitelistedClinic);
}
