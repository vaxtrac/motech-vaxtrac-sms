package org.vaxtrac.motech.sms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.vaxtrac.motech.sms.domain.RelaisLog;
import org.vaxtrac.motech.sms.repository.RelaisLogDataService;
import org.vaxtrac.motech.sms.service.RelaisLogService;

public class RelaisLogServiceImpl implements RelaisLogService {
@Autowired
RelaisLogDataService relaisLogDataService;
	
	public RelaisLogServiceImpl(RelaisLogDataService relaisLogDataService) {
	this.relaisLogDataService = relaisLogDataService;
	}

	public RelaisLogServiceImpl() { }
	
	@Override
	public List<RelaisLog> findByRelaisId(String relaisId) {
		return relaisLogDataService.findByRelaisId(relaisId);
	}

	@Override
	public List<RelaisLog> findByUserId(String userId) {
		return relaisLogDataService.findByUserId(userId);
	}

	@Override
	public List<RelaisLog> findAll() {
		return relaisLogDataService.retrieveAll();
	}

	@Override
	public RelaisLog create(RelaisLog relaisLog) {
		return relaisLogDataService.create(relaisLog);
	}

	@Override
	public void update(RelaisLog relaisLog) {
		relaisLogDataService.update(relaisLog);
	}

	@Override
	public void delete(RelaisLog relaisLog) {
		relaisLogDataService.delete(relaisLog);
	}

	@Override
	public void deleteAll() {
		relaisLogDataService.deleteAll();
	}

	@Override
	public void add(RelaisLog relaisLog) {
		relaisLogDataService.create(relaisLog);
	}

	@Override
	public RelaisLog create(String relaisId, String userId,
			String modificationDate, String field, String oldValue) {
		return relaisLogDataService.create(new RelaisLog(relaisId, userId, modificationDate, field, oldValue));
	}

}
