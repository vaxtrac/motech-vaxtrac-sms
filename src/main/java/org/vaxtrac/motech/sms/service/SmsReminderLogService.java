package org.vaxtrac.motech.sms.service;

import java.util.List;

import org.joda.time.DateTime;
import org.motechproject.commons.api.Range;
import org.vaxtrac.motech.sms.domain.SmsReminderLog;

public interface SmsReminderLogService {
    List<SmsReminderLog> findByRelaisId(String relaisId);
	List<SmsReminderLog> findByPatientId(String patientId);
	List<SmsReminderLog> findByCallbackDate(DateTime callbackDateRange);
	List<SmsReminderLog> findByCallbackDateRange(Range<DateTime> callbackDateRange);
	List<SmsReminderLog> findByReminderDateRange(Range<DateTime> reminderDateRange);
	List<SmsReminderLog> findByIdsAndCallbackDate(String relaisId, String patientId,DateTime callbackDate);
	SmsReminderLog findByIdsAndCallbackDateAndType(String relaisId, String patientId,DateTime callbackDate,String type);
	List<SmsReminderLog> findByType(String type);
	List<SmsReminderLog> findByPatientAndCallbackDate(String patientId,DateTime callbackDate);
	public List<SmsReminderLog> findAll();
    public SmsReminderLog create(SmsReminderLog smsReminderLog);
    public void update(SmsReminderLog smsReminderLog);
    public void delete(SmsReminderLog smsReminderLog);
    public void deleteAll();
    public void add(SmsReminderLog smsReminderLog);
}
