package org.vaxtrac.motech.sms.service.impl;

import java.util.List;

import org.joda.time.DateTime;
import org.motechproject.commons.api.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaxtrac.motech.sms.domain.SmsReminderLog;
import org.vaxtrac.motech.sms.repository.SmsReminderLogDataService;
import org.vaxtrac.motech.sms.service.SmsReminderLogService;

public class SmsReminderLogServiceImpl implements SmsReminderLogService {
	@Autowired
	private SmsReminderLogDataService smsReminderLogDataService;
	
	public SmsReminderLogServiceImpl(
			SmsReminderLogDataService smsReminderLogDataService) {
		this.smsReminderLogDataService = smsReminderLogDataService;
	}
	
	public SmsReminderLogServiceImpl() {
	}

	@Override
	public List<SmsReminderLog> findByRelaisId(String relaisId) {
		return smsReminderLogDataService.findByRelaisId(relaisId);
	}

	@Override
	public List<SmsReminderLog> findByPatientId(String patientId) {
		return smsReminderLogDataService.findByPatientId(patientId);
	}

	@Override
	public List<SmsReminderLog> findByCallbackDate(DateTime callbackDateRange) {
		return smsReminderLogDataService.findByCallbackDate(callbackDateRange);
	}

	@Override
	public List<SmsReminderLog> findByCallbackDateRange(Range<DateTime> callbackDateRange) {
		return smsReminderLogDataService.findByCallbackDateRange(callbackDateRange);
	}

	@Override
	public List<SmsReminderLog> findByReminderDateRange(
			Range<DateTime> reminderDateRange) {
		return smsReminderLogDataService.findByReminderDateRange(reminderDateRange);
	}

	@Override
	public List<SmsReminderLog> findByIdsAndCallbackDate(String relaisId,
			String patientId, DateTime callbackDate) {
		return smsReminderLogDataService.findByIdsAndCallbackDate(relaisId, patientId, callbackDate);
	}

	@Override
	public SmsReminderLog findByIdsAndCallbackDateAndType(String relaisId,
			String patientId, DateTime callbackDate, String type) {
		return smsReminderLogDataService.findByIdsAndCallbackDateAndType(relaisId, patientId, callbackDate, type);
	}

	@Override
	public List<SmsReminderLog> findByType(String type) {
		return smsReminderLogDataService.findByType(type);
	}

	@Override
	public List<SmsReminderLog> findByPatientAndCallbackDate(String patientId,
			DateTime callbackDate) {
		return smsReminderLogDataService.findByPatientAndCallbackDate(patientId, callbackDate);
	}

	@Override
	public List<SmsReminderLog> findAll() {
		return smsReminderLogDataService.retrieveAll();
	}

	@Override
	public SmsReminderLog create(SmsReminderLog smsReminderLog) {
		return smsReminderLogDataService.create(smsReminderLog);
	}

	@Override
	public void update(SmsReminderLog smsReminderLog) {
		smsReminderLogDataService.update(smsReminderLog);
	}

	@Override
	public void delete(SmsReminderLog smsReminderLog) {
		smsReminderLogDataService.delete(smsReminderLog);
	}

	@Override
	public void deleteAll() {
		smsReminderLogDataService.deleteAll();
	}

	@Override
	public void add(SmsReminderLog smsReminderLog) {
		smsReminderLogDataService.create(smsReminderLog);
	}
	
}
