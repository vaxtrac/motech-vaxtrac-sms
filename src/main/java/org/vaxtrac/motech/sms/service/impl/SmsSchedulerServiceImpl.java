package org.vaxtrac.motech.sms.service.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.joda.time.DateTime;
import org.motechproject.commons.api.Range;
import org.motechproject.event.MotechEvent;
import org.motechproject.scheduler.contract.CronJobId;
import org.motechproject.scheduler.contract.CronSchedulableJob;
import org.motechproject.scheduler.service.MotechSchedulerService;
import org.motechproject.sms.service.OutgoingSms;
import org.motechproject.sms.service.SmsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaxtrac.motech.sms.domain.CommcareGuardianCase;
import org.vaxtrac.motech.sms.domain.CommcarePatientCase;
import org.vaxtrac.motech.sms.domain.CommcareVillageMapper;
import org.vaxtrac.motech.sms.domain.Relais;
import org.vaxtrac.motech.sms.domain.RelaisVillage;
import org.vaxtrac.motech.sms.domain.SmsReminderLog;
import org.vaxtrac.motech.sms.event.EventParams;
import org.vaxtrac.motech.sms.event.EventSubjects;
import org.vaxtrac.motech.sms.service.CommcareGuardianService;
import org.vaxtrac.motech.sms.service.CommcarePatientService;
import org.vaxtrac.motech.sms.service.CommcareVillageMapperService;
import org.vaxtrac.motech.sms.service.RelaisService;
import org.vaxtrac.motech.sms.service.SmsReminderLogService;
import org.vaxtrac.motech.sms.service.SmsSchedulerService;

public class SmsSchedulerServiceImpl implements SmsSchedulerService {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private final static String SMS_CONFIG = "plivo_benin";

	private final static int CALLBACK_DATE_RANGE = 7; // numbers of days included in reminders
	private final static int DAYS_BEFORE_CALLBACK = 3; // number of days before the callback date reminders are sent
	private final static int MISSED_APPOINTMENT_RANGE = 10; // number of days before an appointment is considered missed
	private final static String MESSAGE_HEADER = "(VT %d/%d)\n";
	private final static String SUBSCRIPTION_MESSAGE = "Bonjour %s, merci de votre inscription au service de rappels par SMS VaxTrac."; 
	private final static String UNSUBSCRIPTION_MESSAGE = "Bonjour %s, votre désabonnement au service de rappels par SMS VaxTrac est confirmé.";
	private final static String PHONE_UPDATE_MESSAGE = "Bonjour %s, vos modifications ont été prises en compte. VaxTrac.";

	private final static String MISSED_TYPE = "m";
	private final static String UPCOMING_TYPE = "u";

	private final static int WAIT = 1;

	@Autowired
	private RelaisService relaisService;

	@Autowired
	private MotechSchedulerService schedulerService;

	@Autowired
	private SmsService smsService;

	@Autowired
	private SmsReminderLogService smsReminderLogService;

	@Autowired
	private CommcareVillageMapperService commcareVillageMapperService;

	@Autowired
	private CommcarePatientService commcarePatientService;

	@Autowired
	private CommcareGuardianService commcareGuardianService;

	public SmsSchedulerServiceImpl() {
	}

	public SmsSchedulerServiceImpl(RelaisService relaisService,
			MotechSchedulerService schedulerService, SmsService smsService,
			CommcareVillageMapperService commcareVillageMapperService,
			CommcarePatientService commcarePatientService,
			CommcareGuardianService commcareGuardianService,
			SmsReminderLogService smsReminderLogService) {
		this.relaisService = relaisService;
		this.schedulerService = schedulerService;
		this.smsService = smsService;
		this.commcareVillageMapperService = commcareVillageMapperService;
		this.commcarePatientService = commcarePatientService;
		this.commcareGuardianService = commcareGuardianService;
		this.smsReminderLogService = smsReminderLogService;
	}

	@Override
	public void scheduleSendSmsJob(String relaisId, String reminderDays,
			String reminderHour) {
		CronSchedulableJob cronJob = new CronSchedulableJob(prepareMessage(relaisId), 
				buildCronExpression(reminderDays, reminderHour), DateTime.now().toDate(), null, true);
		schedulerService.safeScheduleJob(cronJob);
	}

	@Override
	public void updateSendSmsJob(String relaisId, String reminderDays,
			String reminderHour) {
		try {
			DateTime lastFire = schedulerService.getPreviousFireDate(new CronJobId(EventSubjects.SEND_RELAIS_SMS, relaisId));
			if (lastFire == null) {
				logger.trace("last fire: null");
				unscheduleSendSmsJob(relaisId);
				scheduleSendSmsJob(relaisId, reminderDays, reminderHour);
			}
			else {
				logger.trace("last fire: " + lastFire.toString());
				schedulerService.rescheduleJob(EventSubjects.SEND_RELAIS_SMS, relaisId,
						buildCronExpression(reminderDays, reminderHour));
			}
		}
		catch (Exception e) {
			logger.error("Could not update schedule for relais " + relaisId, e);
		}
	}

	@Override
	public void unscheduleSendSmsJob(String relaisId) {
		schedulerService.safeUnscheduleJob(EventSubjects.SEND_RELAIS_SMS, relaisId);
	}

	private MotechEvent prepareMessage(String caseId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(EventParams.JOB_ID, caseId);
		params.put(EventParams.CASE_ID, caseId);
		return new MotechEvent(EventSubjects.SEND_RELAIS_SMS, params);
	}

	private String buildCronExpression(String reminderDays, String reminderHour) {
		StringBuilder builder = new StringBuilder();
		builder.append("0 ");
		builder.append(reminderHour.substring(3, 5));
		builder.append(" ");
		builder.append(reminderHour.substring(0, 2));
		builder.append(" ? * ");
		builder.append(reminderDays.replace(" ", ","));
		builder.append(" *");
		logger.debug("CRON expr: " + builder.toString());
		return builder.toString();
	}

	@Override
	public void sendReminderSms(String relaisId) throws InterruptedException { //send a sms per patient
		Relais relais = relaisService.findByCaseId(relaisId);
		if (relais != null) {
			List<String> messages = createReminderMessages(relais);
			if (!messages.isEmpty()) {
				int i = 0;
				Iterator<String> it = messages.iterator();
				while (it.hasNext()) {
					i++;
					String message = it.next();
					logger.trace("Relais:" + relaisId + "\n" + message);
					smsService.send(new OutgoingSms(SMS_CONFIG, relais.getPhoneNumber(),
							String.format(MESSAGE_HEADER, i, messages.size()) + message));
					//wait between each sms sent
					TimeUnit.SECONDS.sleep(WAIT);
				}
			}
			else {
				logger.debug("No patients found for relais: " + relaisId);
			}
		}
	}


	//Find patients to include in reminders
	private List<String> createReminderMessages(Relais relais) {
		LinkedList<String> messages = new LinkedList<String>();

		//Date range for children with upcoming appointments
		Range<DateTime> upcomingCallbackRange = new Range<DateTime>(DateTime.now().plusDays(DAYS_BEFORE_CALLBACK).withTime(0, 0, 0, 0),
				DateTime.now().plusDays(DAYS_BEFORE_CALLBACK + CALLBACK_DATE_RANGE - 1).withTime(23, 59, 0, 0));

		//Date range for children with potentially missed appointments
		Range<DateTime> missedCallbackRange = new Range<DateTime>(DateTime.now().minusDays(MISSED_APPOINTMENT_RANGE + CALLBACK_DATE_RANGE).withTime(0, 0, 0, 0),
				DateTime.now().minusDays(MISSED_APPOINTMENT_RANGE + 1).withTime(23, 59, 0, 0));

		Iterator<RelaisVillage> it1 = relais.getVillages().iterator();

		// Final list of patients to send, sorted by ascending callback date
		List<CommcarePatientCase> allPatients = new LinkedList<CommcarePatientCase>();

		//Get all patients
		while (it1.hasNext()) {
			//a village name might have had several spellings over time
			List<CommcareVillageMapper> villages = commcareVillageMapperService.findByVillageId(it1.next().getVillageId());
			//get patients from that village from commcare
			//format patient's info
			if (villages != null && !villages.isEmpty()) {
				for (CommcareVillageMapper village : villages) {
					//Patients who potentially missed an appointment
					List<CommcarePatientCase> patients = commcarePatientService.findRelaisPatients(village.getVillageName(),
							relais.getClinicId(), missedCallbackRange);

					//Remove patients who have already returned
					filterReturnedPatients(patients, relais.getCaseId());
					allPatients.addAll(patients);

					//Patients with an upcoming appointment
					allPatients.addAll(commcarePatientService.findRelaisPatients(village.getVillageName(),
							relais.getClinicId(), upcomingCallbackRange));	
				}
			}
		}

		//Sort patients by callback date
		Collections.sort(allPatients);

		Iterator<CommcarePatientCase> it = allPatients.iterator();

		while (it.hasNext()) {
			CommcarePatientCase patient = it.next();

			//Only send a reminder for a given missed or upcoming appointment once
			String type;
			if (patient.isMissed()) {
				type = MISSED_TYPE;
			}
			else {
				type = UPCOMING_TYPE;
			}
			SmsReminderLog log = smsReminderLogService.findByIdsAndCallbackDateAndType(relais.getCaseId(),
					patient.getCaseId(), patient.getCallbackDate(), type);
			if (log == null) {
				messages.add(createReminderMessage(patient));
				smsReminderLogService.create(new SmsReminderLog(relais.getCaseId(), patient.getCaseId(),
						patient.getCallbackDate(), DateTime.now(), type, patient.getuserId()));
			}
		}
		logger.trace(messages.size() + " messages");
		return messages;
	}

	private String createReminderMessage(CommcarePatientCase patient) {
		StringBuilder builder = new StringBuilder();
		List<CommcareGuardianCase> guardians = commcareGuardianService.findByChildId(patient.getCaseId());
		builder.append(patient.toString());
		builder.append("\nGar: ");
		if (!guardians.isEmpty()) {
			builder.append(guardians.get(0).getGuardianName());
		}
		return builder.toString();
	}


	private void filterReturnedPatients(List<CommcarePatientCase> patients, String relaisId) {
		Iterator<CommcarePatientCase> it = patients.iterator();
		while (it.hasNext()) {
			CommcarePatientCase patient = it.next();
			SmsReminderLog log = smsReminderLogService.findByIdsAndCallbackDateAndType(relaisId,
					patient.getCaseId(),patient.getCallbackDate(), UPCOMING_TYPE);
			if (log != null && log.getReturnDate() != null && !log.getReturnDate().equals("")) {
				//the patient has already returned
				it.remove();
			}
			else {
				//add missed (M) indicator for missed 
				patient.setMissed(true);
			}
		}
	}

	@Override
	public void sendSubscriptionSms(String relaisName, String phoneNumber) {
		smsService.send(new OutgoingSms(SMS_CONFIG, phoneNumber, String.format(SUBSCRIPTION_MESSAGE, relaisName)));
	}

	@Override
	public void sendUnsubscriptionSms(String relaisName, String phoneNumber) {
		smsService.send(new OutgoingSms(SMS_CONFIG, phoneNumber, String.format(UNSUBSCRIPTION_MESSAGE, relaisName)));
	}

	@Override
	public void sendPhoneUpdateSms(String relaisName, String phoneNumber) {
		smsService.send(new OutgoingSms(SMS_CONFIG, phoneNumber, String.format(PHONE_UPDATE_MESSAGE, relaisName)));
	}
}
