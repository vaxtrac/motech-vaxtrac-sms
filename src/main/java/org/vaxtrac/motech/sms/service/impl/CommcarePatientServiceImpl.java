package org.vaxtrac.motech.sms.service.impl;

import java.util.List;

import org.joda.time.DateTime;
import org.motechproject.commons.api.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaxtrac.motech.sms.domain.CommcarePatientCase;
import org.vaxtrac.motech.sms.repository.CommcarePatientDataService;
import org.vaxtrac.motech.sms.service.CommcarePatientService;

public class CommcarePatientServiceImpl implements CommcarePatientService{

	@Autowired
	private CommcarePatientDataService commcarePatientDataService;
	
	public CommcarePatientServiceImpl(
			CommcarePatientDataService commcarePatientDataService) {
		this.commcarePatientDataService = commcarePatientDataService;
	}

	public CommcarePatientServiceImpl() {
	}

	@Override
	public List<CommcarePatientCase> findRelaisPatients(String villageName,
			String clinicId, Range<DateTime> callbackRange) {
		return commcarePatientDataService.findRelaisPatients(villageName, clinicId, callbackRange);
	}

	@Override
	public CommcarePatientCase create(String caseId, String userId,
			String patientName, String contactNumber, String villageName,
			DateTime callbackDate, DateTime lastDoseDate) {
		return commcarePatientDataService.create(new CommcarePatientCase(caseId, userId,
				patientName, contactNumber, villageName, callbackDate, lastDoseDate));
	}

	@Override
	public CommcarePatientCase create() {
		return commcarePatientDataService.create(new CommcarePatientCase());
	}

	@Override
	public void update(CommcarePatientCase patient) {
		commcarePatientDataService.update(patient);
	}

	@Override
	public void delete(CommcarePatientCase patient) {
		commcarePatientDataService.delete(patient);
	}

	@Override
	public CommcarePatientCase findByCaseId(String caseId) {
		return commcarePatientDataService.findByCaseId(caseId);
	}

	@Override
	public List<CommcarePatientCase> findByUserId(String userId) {
		return commcarePatientDataService.findByUserId(userId);
	}

	@Override
	public List<CommcarePatientCase> findByVillage(String villageName,
			String userId) {
		return commcarePatientDataService.findByVillage(villageName, userId);
	}

	@Override
	public CommcarePatientCase create(String caseId, String userId,
			String patientName, String contactNumber, String villageName,
			String callbackDate, DateTime lastDoseDate) {
		return commcarePatientDataService.create(new CommcarePatientCase(caseId, userId,
				patientName, contactNumber, villageName, callbackDate, lastDoseDate));
	}

}
