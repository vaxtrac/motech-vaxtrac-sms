package org.vaxtrac.motech.sms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.vaxtrac.motech.sms.domain.RelaisVillage;
import org.vaxtrac.motech.sms.repository.RelaisVillageDataService;
import org.vaxtrac.motech.sms.service.RelaisVillageService;

public class RelaisVillageServiceImpl implements RelaisVillageService{
	@Autowired
    private RelaisVillageDataService relaisVillageDataService;
	
	public RelaisVillageServiceImpl(RelaisVillageDataService relaisVillageDataService) {
		this.relaisVillageDataService = relaisVillageDataService;
	}

	public RelaisVillageServiceImpl() {
	}

	
	@Override
	public List<RelaisVillage> findAll() {
		return relaisVillageDataService.retrieveAll();
	}

	@Override
	public List<RelaisVillage> findByRelaisId(String relaisId) {
		return relaisVillageDataService.findByRelaisId(relaisId);
	}

	@Override
	public RelaisVillage create(String villageId, String relaisId) {
		return relaisVillageDataService.create(new RelaisVillage(villageId, relaisId));
	}

	@Override
	public void update(RelaisVillage relaisVillage) {
		relaisVillageDataService.update(relaisVillage);
	}

	@Override
	public void delete(RelaisVillage relaisVillage) {
		relaisVillageDataService.delete(relaisVillage);
	}

	@Override
	public void deleteAll() {
		relaisVillageDataService.deleteAll();
	}

	@Override
	public void add(RelaisVillage relaisVillage) {
		relaisVillageDataService.create(relaisVillage);
	}

	@Override
	public RelaisVillage findVillage(String villageId, String relaisId) {
		return relaisVillageDataService.findVillage(villageId, relaisId);
	}

}
