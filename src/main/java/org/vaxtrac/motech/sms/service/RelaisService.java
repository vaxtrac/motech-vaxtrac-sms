package org.vaxtrac.motech.sms.service;

import java.util.List;

import org.vaxtrac.motech.sms.domain.Relais;
import org.vaxtrac.motech.sms.domain.RelaisVillage;

public interface RelaisService {
	public List<Relais> findAll();
    public Relais findByCaseId(String caseId);
    public Relais create(String caseId, String name, String phoneNumber,
			String assignedVillages, String reminderDays, String reminderHour,
			String clinicId);
    public void update(String caseId, String relaisName, String phoneNumber,
			String assignedVillages, String reminderDays, String reminderHour,
			String userId);
    public void delete(Relais relais);
    public void deleteAll();
    public void add(Relais relais);
    public List<RelaisVillage> parseVillages(String assignedVillages, String relaisId);
    public boolean isSameVillageList(List<RelaisVillage> villages1,
			List<RelaisVillage> villages2);
    public List<RelaisVillage> updateVillages(String assignedVillages, Relais relais);
	
}
