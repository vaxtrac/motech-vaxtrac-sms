package org.vaxtrac.motech.sms.service;

public interface SmsSchedulerService {
	public void scheduleSendSmsJob(String relaisId, String reminderDays, String reminderHour);
	public void updateSendSmsJob(String relaisId, String reminderDays, String reminderHour);
	public void unscheduleSendSmsJob(String relaisId);
	public void sendReminderSms(String relaisId) throws InterruptedException;
	public void sendSubscriptionSms(String relaisName, String phoneNumber);
	public void sendUnsubscriptionSms(String relaisName, String phoneNumber);
	public void sendPhoneUpdateSms(String relaisName, String phoneNumber);
}
