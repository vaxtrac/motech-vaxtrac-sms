package org.vaxtrac.motech.sms.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.vaxtrac.motech.sms.service.ComputeSmsReminderDateService;

@Service
public class ComputeSmsReminderDateServiceImpl implements ComputeSmsReminderDateService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private final String CALLBACK_FINISHED = "finished";

    @Override
    public String computeReminderDate(String callbackDate, int offset, int hour, int minutes, String timezoneId) {
	if (!callbackDate.equals(CALLBACK_FINISHED)) {
	    try {
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
       
		Calendar cal = Calendar.getInstance();
		cal.setTime(sf.parse(callbackDate));
		cal.set(Calendar.HOUR_OF_DAY, hour);
		cal.set(Calendar.MINUTE, minutes);
		cal.add(Calendar.DAY_OF_MONTH, -1*offset);
		cal.setTimeZone(TimeZone.getTimeZone(timezoneId));
	
		sf = new SimpleDateFormat("yyyy-MM-dd HH:mm Z");
		logger.info("reminder date " + sf.format(cal.getTime()));
		return sf.format(cal.getTime());
	    } 
	    catch (ParseException e) {
		logger.error("Could not parse callback date " + callbackDate, e);
	    }
	}
	return "";
    }
 
}
