package org.vaxtrac.motech.sms.service;

import java.util.List;

import org.vaxtrac.motech.sms.domain.CommcareVillageMapper;

public interface CommcareVillageMapperService {
    public List<CommcareVillageMapper> findAll();
    public List<CommcareVillageMapper> findByVillageId(String villageId);
    public CommcareVillageMapper findByVillageName(String villageName, String userId);
    public List<CommcareVillageMapper> findByUserId(String userId);
    public CommcareVillageMapper create(String villageId, String villageName, String userId);
    public CommcareVillageMapper create();
    public void update(CommcareVillageMapper village);
    public void delete(CommcareVillageMapper village);
    public void deleteAll();
    public void add(CommcareVillageMapper village);
}
