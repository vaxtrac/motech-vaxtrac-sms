package org.vaxtrac.motech.sms.service;

import java.util.List;

import org.joda.time.DateTime;
import org.motechproject.commons.api.Range;
import org.motechproject.mds.annotations.LookupField;
import org.vaxtrac.motech.sms.domain.CommcarePatientCase;
import org.vaxtrac.motech.sms.domain.CommcareVillageMapper;

public interface CommcarePatientService {
	public CommcarePatientCase findByCaseId(String caseId);
	
	public List<CommcarePatientCase> findByUserId(String userId);
	
	public List<CommcarePatientCase> findByVillage(String villageName, String userId);
	
	public List<CommcarePatientCase> findRelaisPatients(String villageName,
			String clinicId, Range<DateTime> callbackRange);

	public CommcarePatientCase create(String caseId, String userId,
			String patientName, String contactNumber, String villageName,
			DateTime callbackDate, DateTime lastDoseDate);
	
	public CommcarePatientCase create(String caseId, String userId,
			String patientName, String contactNumber, String villageName,
			String callbackDate, DateTime lastDoseDate);
	
	public CommcarePatientCase create();
    public void update(CommcarePatientCase patient);
    public void delete(CommcarePatientCase patient);
}
