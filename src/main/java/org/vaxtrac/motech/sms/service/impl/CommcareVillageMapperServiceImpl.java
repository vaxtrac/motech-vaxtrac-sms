package org.vaxtrac.motech.sms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vaxtrac.motech.sms.domain.CommcareVillageMapper;
import org.vaxtrac.motech.sms.repository.CommcareVillageMapperDataService;
import org.vaxtrac.motech.sms.service.CommcareVillageMapperService;

import java.util.List;

@Service
public class CommcareVillageMapperServiceImpl implements CommcareVillageMapperService{
    @Autowired
    private CommcareVillageMapperDataService commcareVillageMapperDataService;
    
    public CommcareVillageMapperServiceImpl(
			CommcareVillageMapperDataService commcareVillageMapperDataService) {
		this.commcareVillageMapperDataService = commcareVillageMapperDataService;
	}

	public CommcareVillageMapperServiceImpl() {
	}

	@Override
    public List<CommcareVillageMapper> findAll() {
	return commcareVillageMapperDataService.retrieveAll();
    }

    @Override
    public List<CommcareVillageMapper> findByVillageId(String villageId) {
	return commcareVillageMapperDataService.findByVillageId(villageId);
    }

    @Override
    public List<CommcareVillageMapper> findByUserId(String userId) {
	return commcareVillageMapperDataService.findByUserId(userId);
    }

    @Override
    public CommcareVillageMapper findByVillageName(String villageName, String userId) {
	return commcareVillageMapperDataService.findByVillageName(villageName, userId);
    }    

    @Override
    public CommcareVillageMapper create(String villageId, String villageName, String userId) {
    	return commcareVillageMapperDataService.create(new CommcareVillageMapper(villageId, villageName, userId));
    }

    @Override
    public CommcareVillageMapper create() {
	return commcareVillageMapperDataService.create(new CommcareVillageMapper());
    }

    @Override
    public void update(CommcareVillageMapper village) {
	commcareVillageMapperDataService.update(village);
    }

    @Override
    public void delete(CommcareVillageMapper village) {
	commcareVillageMapperDataService.delete(village);	
    }

    @Override
    public void deleteAll() {
	commcareVillageMapperDataService.deleteAll();
    }

    @Override
    public void add(CommcareVillageMapper village) {
	commcareVillageMapperDataService.create(village);
    }

	

}
