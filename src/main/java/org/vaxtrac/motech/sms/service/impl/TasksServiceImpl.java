package org.vaxtrac.motech.sms.service.impl;

import org.vaxtrac.motech.sms.service.TasksService;
import org.vaxtrac.motech.sms.tasks.builder.ChannelRequestBuilder;
import org.motechproject.tasks.contract.ChannelRequest;
import org.motechproject.tasks.service.ChannelService;
import org.osgi.framework.BundleContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TasksServiceImpl implements TasksService {

	private BundleContext bundleContext;
	private ChannelService channelService;
    private ChannelRequest channelRequest;

	@Autowired
    public TasksServiceImpl(BundleContext bundleContext, ChannelService channelService) {
        this.bundleContext = bundleContext;
        this.channelService = channelService;
    }
	
	@Override
	public void updateChannel() {
		ChannelRequestBuilder channelRequestBuilder = new ChannelRequestBuilder(bundleContext);
		channelRequest = channelRequestBuilder.build();
		channelService.registerChannel(channelRequest);
	}
    
    @Override
    public void removeChannel(){
	channelService.unregisterChannel(channelRequest.getModuleName());
    }
}
