package org.vaxtrac.motech.sms.service;

public interface TasksService {

    /**
     * Builds a channel request from the information saved in MDS and then updates the channel for this
     * module.
     */
    void updateChannel();
    void removeChannel();
}
