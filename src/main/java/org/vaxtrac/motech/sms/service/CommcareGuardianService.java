package org.vaxtrac.motech.sms.service;

import java.util.List;

import org.motechproject.mds.annotations.Lookup;
import org.motechproject.mds.annotations.LookupField;
import org.vaxtrac.motech.sms.domain.CommcareGuardianCase;

public interface CommcareGuardianService {
	public List<CommcareGuardianCase> findByChildId(String childId);
	public CommcareGuardianCase findByCaseId(String caseId);
    public List<CommcareGuardianCase> findByUserId(String userId);
	
	public CommcareGuardianCase create(String caseId, String userId,
			String guardianName, String childId);
	public CommcareGuardianCase create();
    public void update(CommcareGuardianCase guardian);
    public void delete(CommcareGuardianCase guardian);
}
