package org.vaxtrac.motech.sms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.vaxtrac.motech.sms.domain.CommcareGuardianCase;
import org.vaxtrac.motech.sms.repository.CommcareGuardianDataService;
import org.vaxtrac.motech.sms.service.CommcareGuardianService;

public class CommcareGuardianServiceImpl implements CommcareGuardianService{
	@Autowired
	private CommcareGuardianDataService commcareGuardianDataService;
	
	public CommcareGuardianServiceImpl(
			CommcareGuardianDataService commcareGuardianDataService) {
		this.commcareGuardianDataService = commcareGuardianDataService;
	}
	
	public CommcareGuardianServiceImpl() {
	}
	
	@Override
	public List<CommcareGuardianCase> findByChildId(String childId) {
		return commcareGuardianDataService.findByChildId(childId);
	}

	@Override
	public CommcareGuardianCase create(String caseId, String userId,
			String guardianName, String childId) {
		return commcareGuardianDataService.create(new CommcareGuardianCase(caseId, userId, guardianName, childId));
	}

	@Override
	public CommcareGuardianCase create() {
		return commcareGuardianDataService.create(new CommcareGuardianCase());
	}

	@Override
	public void update(CommcareGuardianCase guardian) {
		commcareGuardianDataService.update(guardian);
	}

	@Override
	public void delete(CommcareGuardianCase guardian) {
		commcareGuardianDataService.delete(guardian);
	}

	@Override
	public CommcareGuardianCase findByCaseId(String caseId) {
		return commcareGuardianDataService.findByCaseId(caseId);
	}

	@Override
	public List<CommcareGuardianCase> findByUserId(String userId) {
		return commcareGuardianDataService.findByUserId(userId);
	}

}
