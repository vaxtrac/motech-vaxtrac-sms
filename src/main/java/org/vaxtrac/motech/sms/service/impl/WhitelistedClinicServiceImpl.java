package org.vaxtrac.motech.sms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.vaxtrac.motech.sms.domain.WhitelistedClinic;
import org.vaxtrac.motech.sms.repository.WhitelistedClinicDataService;
import org.vaxtrac.motech.sms.service.WhitelistedClinicService;

public class WhitelistedClinicServiceImpl implements WhitelistedClinicService {
	@Autowired
	private WhitelistedClinicDataService whitelistedClinicDataService;
	
	public WhitelistedClinicServiceImpl(
			WhitelistedClinicDataService whitelistedClinicDataService) {
		this.whitelistedClinicDataService = whitelistedClinicDataService;
	}
	
	public WhitelistedClinicServiceImpl() { }

	@Override
	public WhitelistedClinic findByClinicId(String clinicId) {
		return whitelistedClinicDataService.findByClinicId(clinicId);
	}

	@Override
	public List<WhitelistedClinic> findAll() {
		return whitelistedClinicDataService.retrieveAll();
	}

	@Override
	public WhitelistedClinic create(WhitelistedClinic whitelistedClinic) {
		return whitelistedClinicDataService.create(whitelistedClinic);
	}

	@Override
	public void update(WhitelistedClinic whitelistedClinic) {
		whitelistedClinicDataService.update(whitelistedClinic);
	}

	@Override
	public void delete(WhitelistedClinic whitelistedClinic) {
		whitelistedClinicDataService.delete(whitelistedClinic);
	}

	@Override
	public void deleteAll() {
		whitelistedClinicDataService.deleteAll();
	}

	@Override
	public void add(WhitelistedClinic whitelistedClinic) {
		whitelistedClinicDataService.create(whitelistedClinic);
	}
}
