package org.vaxtrac.motech.sms.service.impl;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vaxtrac.motech.sms.domain.Relais;
import org.vaxtrac.motech.sms.domain.RelaisVillage;
import org.vaxtrac.motech.sms.repository.RelaisDataService;
import org.vaxtrac.motech.sms.service.RelaisService;
import org.vaxtrac.motech.sms.service.RelaisVillageService;

@Service
public class RelaisServiceImpl implements RelaisService {
	@Autowired
	private RelaisDataService relaisDataService;

	@Autowired
	private RelaisVillageService relaisVillageService;

	public RelaisServiceImpl(RelaisDataService relaisDataService, RelaisVillageService relaisVillageService) {
		this.relaisDataService = relaisDataService;
		this.relaisVillageService = relaisVillageService;
	}

	public RelaisServiceImpl() {
	}

	@Override
	public List<Relais> findAll() {
		return relaisDataService.retrieveAll();
	}

	@Override
	public Relais findByCaseId(String caseId) {
		return relaisDataService.findByCaseId(caseId);
	}

	@Override
	public Relais create(String caseId, String name, String phoneNumber,
			String assignedVillages, String reminderDays, String reminderHour,
			String clinicId) {
		List<RelaisVillage> villages = parseVillages(assignedVillages, caseId);
		Relais relais = relaisDataService.create(new Relais(caseId, name, phoneNumber, villages, 
				reminderDays, reminderHour, clinicId));

		return relais;
	}

	@Override
	public boolean isSameVillageList(List<RelaisVillage> villages1, List<RelaisVillage> villages2) {
		return (villages1.containsAll(villages2) && villages2.containsAll(villages1));
	}

	@Override
	public List<RelaisVillage> parseVillages(String assignedVillages, String relaisId) {
		//parse assigned villages into a list
		String[] villages = assignedVillages.split(" ");
		List<RelaisVillage> res = new LinkedList<RelaisVillage>();
		for (int i=0; i<villages.length; i++) {
			res.add(new RelaisVillage(villages[i], relaisId));
		}
		return res;
	}

	@Override
	public List<RelaisVillage> updateVillages(String assignedVillages, Relais relais) {
		List<RelaisVillage> villagesToDelete = new LinkedList<RelaisVillage>();

		if (!assignedVillages.equals("")) {
			String[] villages = assignedVillages.split(" ");

			List<RelaisVillage> currentVillages = relais.getVillages();

			//Add new villages
			for (int i=0; i<villages.length; i++) {
				RelaisVillage village = relaisVillageService.findVillage(villages[i], relais.getCaseId());
				if (village == null) 
					village = relaisVillageService.create(villages[i], relais.getCaseId());
				currentVillages.add(village);
			}

			//Remove villages that are not in the new list
			Iterator<RelaisVillage> it = currentVillages.iterator();
			while (it.hasNext()) {
				boolean remove = true;
				RelaisVillage village = it.next();
				for (int i=0; i<villages.length; i++) {
					if (village.getVillageId().equals(villages[i]))
						remove = false;
				}
				if (remove) {
					RelaisVillage vil = relaisVillageService.findVillage(village.getVillageId(), relais.getCaseId());
					if (vil != null)
						villagesToDelete.add(vil);
					it.remove();
				}
			}
		}
		return villagesToDelete;
	}


	@Override
	public void delete(Relais relais) {
		List<RelaisVillage> villages = relaisVillageService.findByRelaisId(relais.getCaseId());
		Iterator<RelaisVillage> ite = villages.iterator();
		while (ite.hasNext()) {
			relaisVillageService.delete(ite.next());
		}
		relaisDataService.delete(relais);
	}

	@Override
	public void deleteAll() {
		relaisDataService.deleteAll();
	}

	@Override
	public void add(Relais relais) {
		relaisDataService.create(relais);
	}

	@Override
	public void update(String caseId, String relaisName, String phoneNumber,
			String assignedVillages, String reminderDays, String reminderHour,
			String userId) {
		Relais relais = findByCaseId(caseId);
		if (relais != null) {
			relais.setClinicId(userId);
			relais.setName(relaisName);
			relais.setPhoneNumber(phoneNumber);
			relais.setReminderDays(reminderDays);
			relais.setReminderHour(reminderHour);
			List<RelaisVillage> toDelete = updateVillages(assignedVillages, relais);
			relaisDataService.update(relais);

			Iterator<RelaisVillage> it = toDelete.iterator();
			while (it.hasNext())
				relaisVillageService.delete(it.next());
		}
	}

}
