package org.vaxtrac.motech.sms.parser;

import java.util.List;

public interface VaccineParser {
	List<Vaccine> parseVaccines(String jsonVaccines);
	Vaccine findMostRecentDose(String jsonVaccines);
}
