package org.vaxtrac.motech.sms.parser;

import org.joda.time.DateTime;

public class Vaccine implements Comparable<Vaccine> {
    private String name;
    private int series;  
    private DateTime date;
  
    public Vaccine (String name, DateTime date, int series) {
	this.name = name;
	this.date = date;
	this.series = series;
    }
	
    public String getName(){
	return name;
    }
	
    public DateTime getDate() {
	return date;
    }
	
    public int getSeries() {
	return series;
    }

    public String toString() {
	return name + series + " : " + date.toString();
    }

	@Override
	public int compareTo(Vaccine other) {
		//sort in descending order
		return -1 * this.date.compareTo(other.date);
	}
}
