package org.vaxtrac.motech.sms.parser;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VaccineParserImpl implements VaccineParser{

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public List<Vaccine> parseVaccines(String jsonVaccines) {
		List<Vaccine> vaccines = new LinkedList<Vaccine>();

		if (!jsonVaccines.equals("") && jsonVaccines!=null) {

			try{
				JSONObject obj = new JSONObject(jsonVaccines);
				Iterator<String> it = obj.keys();
				while (it.hasNext()) {
					String vaccineName = it.next();
					JSONArray arr = obj.getJSONArray(vaccineName);
					for (int i=0; i<arr.length(); i++) {
						String text = arr.getString(i);
						DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
						DateTime date = formatter.parseDateTime(text);
						vaccines.add(new Vaccine(vaccineName, date, i));
					}
				}
			}catch(Exception e){
				logger.error("Could not parse vaccine date", e);
			}
		}
		return vaccines;
	}

	@Override
	public Vaccine findMostRecentDose(String jsonVaccines) {
		List<Vaccine> vaccines =  parseVaccines(jsonVaccines);
		Collections.sort(vaccines);
		if (!vaccines.isEmpty())
			return vaccines.get(0);
		else
			return null;
	}


}
