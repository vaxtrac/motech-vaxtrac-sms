package org.vaxtrac.motech.sms.repository;

import org.vaxtrac.motech.sms.domain.CommcarePatientCase;

import java.util.List;

import org.joda.time.DateTime;
import org.motechproject.commons.api.Range;
import org.motechproject.mds.annotations.Lookup;
import org.motechproject.mds.annotations.LookupField;
import org.motechproject.mds.service.MotechDataService;

public interface CommcarePatientDataService extends MotechDataService<CommcarePatientCase> {
	@Lookup
	CommcarePatientCase findByCaseId(@LookupField(name = "caseId") String caseId);
	
	@Lookup
    List<CommcarePatientCase> findByUserId(@LookupField(name = "userId") String userId);
	
	@Lookup
    List<CommcarePatientCase> findByVillage(@LookupField(name = "villageName") String villageName, @LookupField(name = "userId") String userId);
	
	@Lookup
	List<CommcarePatientCase> findRelaisPatients(@LookupField(name = "villageName") String villageName, @LookupField(name = "userId") String userId,
			@LookupField(name = "callbackDate") Range<DateTime> callbackDateRange);
}
