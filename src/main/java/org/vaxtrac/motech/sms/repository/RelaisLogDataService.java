package org.vaxtrac.motech.sms.repository;

import java.util.List;

import org.motechproject.mds.annotations.Lookup;
import org.motechproject.mds.annotations.LookupField;
import org.motechproject.mds.service.MotechDataService;
import org.vaxtrac.motech.sms.domain.RelaisLog;

public interface RelaisLogDataService extends MotechDataService<RelaisLog> {
	@Lookup
    List<RelaisLog> findByRelaisId(@LookupField(name = "relaisId") String relaisId);
	
	@Lookup
    List<RelaisLog> findByUserId(@LookupField(name = "userId") String userId);
}
