package org.vaxtrac.motech.sms.repository;

import org.motechproject.mds.annotations.Lookup;
import org.motechproject.mds.annotations.LookupField;
import org.motechproject.mds.service.MotechDataService;
import org.vaxtrac.motech.sms.domain.WhitelistedClinic;

public interface WhitelistedClinicDataService extends MotechDataService<WhitelistedClinic> {
	@Lookup
	WhitelistedClinic findByClinicId(@LookupField(name = "clinicId") String clinicId);
}
