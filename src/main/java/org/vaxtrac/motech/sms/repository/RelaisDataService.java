package org.vaxtrac.motech.sms.repository;

import org.vaxtrac.motech.sms.domain.Relais;
import org.motechproject.mds.annotations.Lookup;
import org.motechproject.mds.annotations.LookupField;
import org.motechproject.mds.service.MotechDataService;

public interface RelaisDataService extends MotechDataService<Relais> {
	@Lookup
    Relais findByCaseId(@LookupField(name = "caseId") String caseId);
}
