package org.vaxtrac.motech.sms.repository;

import java.util.List;

import org.motechproject.mds.annotations.Lookup;
import org.motechproject.mds.annotations.LookupField;
import org.motechproject.mds.service.MotechDataService;
import org.vaxtrac.motech.sms.domain.RelaisVillage;


public interface RelaisVillageDataService extends MotechDataService<RelaisVillage> {
	@Lookup
    List<RelaisVillage> findByRelaisId(@LookupField(name = "relaisId") String relaisId);
	
	@Lookup
    RelaisVillage findVillage(@LookupField(name = "villageId") String villageId, @LookupField(name = "relaisId") String relaisId);
}
