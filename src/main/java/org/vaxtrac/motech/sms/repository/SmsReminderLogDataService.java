package org.vaxtrac.motech.sms.repository;

import java.util.List;

import org.joda.time.DateTime;
import org.motechproject.commons.api.Range;
import org.motechproject.mds.annotations.Lookup;
import org.motechproject.mds.annotations.LookupField;
import org.motechproject.mds.service.MotechDataService;
import org.vaxtrac.motech.sms.domain.SmsReminderLog;

public interface SmsReminderLogDataService extends MotechDataService<SmsReminderLog>{
	@Lookup
    List<SmsReminderLog> findByRelaisId(@LookupField(name = "relaisId") String relaisId);
	
	@Lookup
	List<SmsReminderLog> findByPatientId(@LookupField(name = "patientId") String patientId);
	
	@Lookup
	List<SmsReminderLog> findByCallbackDate(@LookupField(name = "callbackDate") DateTime callbackDateRange);
	
	@Lookup
	List<SmsReminderLog> findByCallbackDateRange(@LookupField(name = "callbackDate") Range<DateTime> callbackDateRange);
	
	@Lookup
	List<SmsReminderLog> findByReminderDateRange(@LookupField(name = "reminderDate") Range<DateTime> reminderDateRange);
	
	@Lookup
	List<SmsReminderLog> findByIdsAndCallbackDate(@LookupField(name = "relaisId") String relaisId, 
			@LookupField(name = "patientId") String patientId,
			@LookupField(name = "callbackDate") DateTime callbackDate);
	
	@Lookup
	SmsReminderLog findByIdsAndCallbackDateAndType(@LookupField(name = "relaisId") String relaisId, 
			@LookupField(name = "patientId") String patientId,
			@LookupField(name = "callbackDate") DateTime callbackDate,
			@LookupField(name = "type") String type);
	
	@Lookup
	List<SmsReminderLog> findByType(@LookupField(name = "type") String type);
	
	@Lookup
	List<SmsReminderLog> findByPatientAndCallbackDate( 
			@LookupField(name = "patientId") String patientId,
			@LookupField(name = "callbackDate") DateTime callbackDate);
}