package org.vaxtrac.motech.sms.repository;

import java.util.List;

import org.vaxtrac.motech.sms.domain.CommcareVillageMapper;
import org.motechproject.mds.annotations.Lookup;
import org.motechproject.mds.annotations.LookupField;
import org.motechproject.mds.service.MotechDataService;

public interface CommcareVillageMapperDataService extends MotechDataService<CommcareVillageMapper>{
    @Lookup
    List<CommcareVillageMapper> findByVillageId(@LookupField(name = "villageId") String villageId);

    @Lookup
    CommcareVillageMapper findByVillageName(@LookupField(name = "villageName") String villageName,
					    @LookupField(name = "userId") String userId);

    @Lookup
    List<CommcareVillageMapper> findByUserId(@LookupField(name = "userId") String userId);

}
