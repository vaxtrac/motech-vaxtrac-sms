package org.vaxtrac.motech.sms.repository;

import org.vaxtrac.motech.sms.domain.CommcareGuardianCase;

import java.util.List;

import org.motechproject.mds.annotations.Lookup;
import org.motechproject.mds.annotations.LookupField;
import org.motechproject.mds.service.MotechDataService;

public interface CommcareGuardianDataService extends MotechDataService<CommcareGuardianCase> {
	@Lookup
    CommcareGuardianCase findByCaseId(@LookupField(name = "caseId") String caseId);
	
	@Lookup
    List<CommcareGuardianCase> findByChildId(@LookupField(name = "childId") String childId);
	
	@Lookup
    List<CommcareGuardianCase> findByUserId(@LookupField(name = "userId") String userId);
}
